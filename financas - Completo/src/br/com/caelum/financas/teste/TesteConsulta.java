package br.com.caelum.financas.teste;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.Query;

import br.com.caelum.financas.modelo.Conta;
import br.com.caelum.financas.modelo.Movimentacao;
import br.com.caelum.financas.util.JPAUtil;

public class TesteConsulta {

	public static void main(String[] args) {
		
		EntityManager manager = new JPAUtil().getEntityManager();
		
		Conta conta = new Conta();
		conta.setId(1);
		
		try{
			//Query query = manager.createQuery("select m from Movimentacao m where m.conta = ?1");
			//query.setParameter(1, conta);
			
			Query query = manager.createQuery("select m from Movimentacao m where m.conta =:pConta");
			query.setParameter("pConta", conta);
			
			
			List<Movimentacao> movimetnacoes = query.getResultList();
			
			for(Movimentacao m : movimetnacoes){
				System.out.println("Descricao: " + m.getDescricao());
				System.out.println("Valor: " + m.getValor());
				
			}
		
		}catch(Exception e){
			System.out.println(e);
		}
		
	}
	
}
