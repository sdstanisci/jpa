package br.com.caelum.financas.teste;

import javax.persistence.EntityManager;
import br.com.caelum.financas.modelo.Conta;
import br.com.caelum.financas.modelo.TipoMovimentacao;
import br.com.caelum.financas.util.JPAUtil;

public class TesteConsultaFuncoes {

	public static void main(String[] args) {

		EntityManager manager = new JPAUtil().getEntityManager();

		Conta conta = new Conta();
		conta.setId(1);

		// 1 forma de buscar informacoes, por createQuery
		MovimentacaoDao movimentacaoDao = new MovimentacaoDao(manager);
		double media = movimentacaoDao.mediaDoCalculoPeloTipo(conta,TipoMovimentacao.SAIDA);
		System.out.println("Media: "+media);
		
		//2 forma de buscar informacoes, por createNamedQuery
		media = movimentacaoDao.consultaNamedQuery(conta,TipoMovimentacao.ENTRADA);
		System.out.println("Media: "+media);
		
		
	}

}
	