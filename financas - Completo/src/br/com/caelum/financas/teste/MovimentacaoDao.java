package br.com.caelum.financas.teste;

import javax.persistence.EntityManager;
import javax.persistence.TypedQuery;

import br.com.caelum.financas.modelo.Conta;
import br.com.caelum.financas.modelo.TipoMovimentacao;

public class MovimentacaoDao {

	private EntityManager manager;
	
	public MovimentacaoDao(EntityManager manager){
		this.manager = manager;
	}
	
	public Double consultaNamedQuery(Conta conta, TipoMovimentacao tipo){
		
		TypedQuery<Double> query = manager.createNamedQuery("mediaDaContaPeloTipoMovimentacao", Double.class);
				
		query.setParameter("pConta", conta);
		query.setParameter("pTipo", tipo);
		
		Double media = query.getSingleResult();
		return media;
	}
	
	public Double mediaDoCalculoPeloTipo(Conta conta, TipoMovimentacao tipo){
		
		TypedQuery<Double> query = manager.createQuery("select avg(m.valor) from Movimentacao m where m.conta = :pConta"
						+ " and m.tipoMovimentacao = :pTipo"
						+ " order by m.valor desc", Double.class);
		
		
		query.setParameter("pConta", conta);
		query.setParameter("pTipo", tipo);
		
		Double media = query.getSingleResult();
		
		// passando 2 parametros com numeros de posicoes
		/*Query query = manager.createQuery("select m from Movimentacao m where m.conta=?1"
		 				+ " and m.tipoMovimentacao=?2");

		query.setParameter(1, conta);
		query.setParameter(2, TipoMovimentacao.ENTRADA);

		Query query = manager.createQuery("select sum(m.valor) from Movimentacao m where m.conta = :pConta"
						+ " and m.tipoMovimentacao = :pTipo"
						+ " order by m.valor desc");*/

		// query com o sum, retorna um bigdecimal
		/*TypedQuery<BigDecimal> query = manager
				.createQuery("select sum(m.valor) from Movimentacao m where m.conta = :pConta"
						+ " and m.tipoMovimentacao = :pTipo"
						+ " order by m.valor desc", BigDecimal.class);
		
		
		query.setParameter("pConta", conta);
		query.setParameter("pTipo", TipoMovimentacao.SAIDA);*/
		
		// executa a query
		/*List<Movimentacao> movimentacoes = query.getResultList();
		
		//retorna lista com resultado
		BigDecimal soma = query.getSingleResult();
		System.out.println("Soma: "+soma);*/

		return media;
	}
	
}
