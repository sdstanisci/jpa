package br.com.caelum.financas.teste;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

import br.com.caelum.financas.modelo.Conta;

public class TesteJPA {
		
	public static void main(String args[]){
	
		Conta conta = new Conta();
		conta.setTitular("Sergio Stanisci");
		conta.setBanco("Itau");
		conta.setNumero("08364-0");
		conta.setAgencia("4640");
		
		try{
		EntityManagerFactory entityManagerFactory = Persistence.createEntityManagerFactory("financas");
		EntityManager manager = entityManagerFactory.createEntityManager();
		
		manager.getTransaction().begin();
		manager.persist(conta);
		
		conta.setTitular("Sergio Atualizado");
		
		manager.getTransaction().commit();
		manager.close();
		}catch(Exception e){
			System.out.println(e);
		}
	}
		
}
