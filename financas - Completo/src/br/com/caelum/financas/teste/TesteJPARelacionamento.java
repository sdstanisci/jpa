package br.com.caelum.financas.teste;

import java.math.BigDecimal;
import java.util.Calendar;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

import br.com.caelum.financas.modelo.Conta;
import br.com.caelum.financas.modelo.Movimentacao;
import br.com.caelum.financas.modelo.TipoMovimentacao;

public class TesteJPARelacionamento {
		
	public static void main(String args[]){
	
		Conta conta = new Conta();
		//conta.setId(1);
		conta.setTitular("Sergio Stanisci");
		conta.setBanco("Itau");
		conta.setNumero("08364-0");
		conta.setAgencia("4640");
		
		Movimentacao movimentacao = new Movimentacao();
		movimentacao.setData(Calendar.getInstance());
		movimentacao.setDescricao("Conta de Luz");
		movimentacao.setTipoMovimentacao(TipoMovimentacao.SAIDA);
		movimentacao.setValor(new BigDecimal("123.90"));
		
		movimentacao.setConta(conta);
		
		try{
		EntityManagerFactory entityManagerFactory = Persistence.createEntityManagerFactory("financas");
		EntityManager manager = entityManagerFactory.createEntityManager();
		
		manager.getTransaction().begin();
		
		manager.persist(conta);
		manager.persist(movimentacao);
		
		
		manager.getTransaction().commit();
		manager.close();
		}catch(Exception e){
			System.out.println(e);
		}
	}
		
}
