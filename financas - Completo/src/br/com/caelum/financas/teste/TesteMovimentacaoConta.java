package br.com.caelum.financas.teste;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.Query;

import br.com.caelum.financas.modelo.Conta;
import br.com.caelum.financas.modelo.Movimentacao;
import br.com.caelum.financas.util.JPAUtil;

public class TesteMovimentacaoConta {

	public static void main(String[] args) {
		
		EntityManager manager = new JPAUtil().getEntityManager();
		/*Movimentacao movimentacao = manager.find(Movimentacao.class, 1);
		System.out.println("Titular:" + movimentacao.getConta().getTitular());*/
		
		
		/*Conta conta = manager.find(Conta.class, 1);
		System.out.println("Qnt. Movimentacoes:" + conta.getMovimentacoes().size());*/
		
		// se rodar dessa forma vai montar um query para cada movimentacao
		/*Query query = manager.createQuery("select c from Conta c");
		List<Conta> contas = query.getResultList();*/
		
		// se rodar dessa forma vai forcar a fazer o join com o list de movimentacoes que esta denx'tro de conta com isso montando apenas uma quey
		Query query = manager.createQuery("select c from Conta c join fetch c.movimentacoes");
		List<Conta> contas = query.getResultList();
		
		for(Conta conta : contas){
			System.out.println("Num movimentacoes: " + conta.getMovimentacoes().size());
		}
		
		manager.close();
	}

}
