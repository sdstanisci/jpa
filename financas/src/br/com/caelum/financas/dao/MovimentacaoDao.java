package br.com.caelum.financas.dao;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.TypedQuery;

import br.com.caelum.financas.enuns.TipoMovimentacao;
import br.com.caelum.financas.modelo.Conta;
import br.com.caelum.financas.modelo.Movimentacao;
import br.com.caelum.financas.util.JPAUtil;

public class MovimentacaoDao {

	private EntityManager em;
	
	public MovimentacaoDao() {
		em = new JPAUtil().getEntityManager();
	}
	
	public List<Double> getMediasPorDiaETipo(Enum<TipoMovimentacao> tipoMovimentacao, Conta conta){
		
		em.getTransaction().begin();
		
		String jpql = "select distinct avg(m.valor) from Movimentacao m where m.conta = :pConta "
				+ " and m.tipo = :pTipo"
				+ " group by m.data ";
				
		TypedQuery<Double> query = em.createQuery(jpql, Double.class);
		query.setParameter("pConta", conta);
		query.setParameter("pTipo", TipoMovimentacao.SAIDA);
		
		List<Double> medias =  query.getResultList();
		
		em.getTransaction().commit();
		em.close();
		
		JPAUtil.closeEntityManager();
		
		return medias;
	}
}
