package br.com.caelum.financas.enuns;

public enum TipoMovimentacao {

	ENTRADA,
	SAIDA;
}
