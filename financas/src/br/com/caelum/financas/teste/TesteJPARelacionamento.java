package br.com.caelum.financas.teste;

import java.math.BigDecimal;
import java.util.Calendar;

import javax.persistence.EntityManager;

import br.com.caelum.financas.enuns.TipoMovimentacao;
import br.com.caelum.financas.modelo.Conta;
import br.com.caelum.financas.modelo.Movimentacao;
import br.com.caelum.financas.util.JPAUtil;

public class TesteJPARelacionamento {

	public static void main(String[] args) {
		
		Movimentacao movimentacao = new Movimentacao();
		movimentacao.setData(Calendar.getInstance());
		movimentacao.setDescricao("Churrascaria");
		movimentacao.setTipo(TipoMovimentacao.SAIDA);
		movimentacao.setValor(new BigDecimal("200.00"));
		
		Conta conta = new Conta();
		conta.setTitular("Nick Bastista");
		conta.setBanco("Bradesco Poupan�a");
		conta.setAgencia("123");
		conta.setNumero("456");
		
		movimentacao.setConta(conta);
		
		
		EntityManager em = new JPAUtil().getEntityManager();
		// Entra no estado de Transient ate esse ponto
		em.getTransaction().begin();
		// Entra no estado de managed a partir deste ponto
		
		// apresenta exce��o pois somente a movimentacao esta em managed, precisa trazer a conta tmb
		//em.persist(movimentacao);
		
		em.persist(conta);
		em.persist(movimentacao);
		
		em.getTransaction().commit();
		em.close();
	}
	
}
