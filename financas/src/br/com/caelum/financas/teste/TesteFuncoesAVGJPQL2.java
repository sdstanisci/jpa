package br.com.caelum.financas.teste;

import java.util.List;
import br.com.caelum.financas.dao.MovimentacaoDao;
import br.com.caelum.financas.enuns.TipoMovimentacao;
import br.com.caelum.financas.modelo.Conta;

public class TesteFuncoesAVGJPQL2 {

	public static void main(String[] args) {
		
		Conta conta = new Conta();
		conta.setId(1);
		
		MovimentacaoDao dao = new MovimentacaoDao();
		List<Double> medias = dao.getMediasPorDiaETipo(TipoMovimentacao.SAIDA, conta);
		
		for (Double media : medias) {
			System.out.println("media: " + media	);
		}
		
	}

}
