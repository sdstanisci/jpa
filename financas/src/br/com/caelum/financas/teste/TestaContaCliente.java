package br.com.caelum.financas.teste;

import javax.persistence.EntityManager;
import javax.persistence.JoinColumn;

import br.com.caelum.financas.modelo.Cliente;
import br.com.caelum.financas.modelo.Conta;
import br.com.caelum.financas.util.JPAUtil;

public class TestaContaCliente {
	
	public static void main(String[] args) {
		
		Cliente cliente = new Cliente();
		cliente.setNome("Nick");
		cliente.setEndereco("Rua Andre Pujos, 57 Apto 65");
		cliente.setProfissao("Bebe");
				
		Conta conta = new Conta();
		conta.setId(1);
		
		cliente.setConta(conta);
		
		// ---------------------------------------------------
		
		//Teste de unique nao funciona sem a anotacao @JoinColumn(unique=true)
		/*Cliente cliente2 = new Cliente();
		cliente2.setNome("Nick");
		cliente2.setEndereco("Rua Andre Pujos, 57 Apto 65");
		cliente2.setProfissao("Bebe");
				
		Conta conta2 = new Conta();
		conta2.setId(1);
		
		cliente2.setConta(conta2);*/
		
		EntityManager em = new JPAUtil().getEntityManager();
		em.getTransaction().begin();
		
		em.persist(cliente);
		
		//em.persist(cliente2);
		
		
		em.getTransaction().commit();
		em.close();
	}
}
