package br.com.caelum.financas.teste;

import javax.persistence.EntityManager;
import br.com.caelum.financas.modelo.Conta;
import br.com.caelum.financas.util.JPAUtil;

public class TesteConta {

	public static void main(String[] args) {
		
		Conta conta = new Conta();
		
		conta.setTitular("Gabriela Batista");
		conta.setBanco("Itau");
		conta.setAgencia("123");
		conta.setNumero("456");
		
		JPAUtil jpaUtil = new JPAUtil();
		EntityManager em = jpaUtil.getEntityManager();
		
		// Entra no estado de Transient ate esse ponto
		em.getTransaction().begin();
		// Entra no estado de managed a partir deste ponto
		em.persist(conta);
		
		conta.setBanco("Brasil");
		
		em.getTransaction().commit();
		em.close();
		
		// Demonstrando o Merge
		EntityManager em2 = new JPAUtil().getEntityManager();
		
		// Entra no estado Detached, pois o gerenciamento do JPA foi matado no em.close();
		em2.getTransaction().begin();
		conta.setTitular("Sergio");
		//em2.persist(conta);
		
		// Para atualizar a conta, precisa ser pelo Merge, a JPA retorna o gerenciamento ao estado de managed
		em2.merge(conta);
		
		em2.getTransaction().commit();
		em2.close();
		
		// Demonstrando o remove
		EntityManager em3 = new JPAUtil().getEntityManager();
		
		em3.getTransaction().begin();
		
		// para remover precisa recuperar o objeto/registro do banco e coloca-lo no estado de managed para a JPA poder gerenciar
		
		conta = em3.find(Conta.class, 13);
		em3.remove(conta);
		
		em3.getTransaction().commit();
		em3.close();
		
		jpaUtil.closeEntityManager();
	}
}
